
"""
From Xiavan Roopnarinesingh to Everyone:  01:15 PM
https://gitlab.com/xroopnar/mana-dl
From chase to Everyone:  02:26 PM
https://gitlab.com/chase_brown/phemrl_dl-codejam/-/blob/master/PHEMRL.py
From Cory Giles to Everyone:  02:30 PM
http://www.pollandmatch.com/chooser?pollId=57b79a5f-9d23-424b-a1ca-8ed3cf7a5be2
"""

print("starting fdl_main_cnn_code_jam.py")
print()


import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rc("figure", facecolor="white")

import seaborn as sns
sns.set_style('whitegrid')
sns.set_palette("bright")


# Deep Learning Libraries
#from keras.models import load_model
from keras.layers import BatchNormalization
from keras.optimizers import Adam, SGD, Adagrad, Adadelta, RMSprop
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau, LearningRateScheduler
from keras.utils import to_categorical

import tensorflow as tf

from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import  Dropout, Dense,GlobalMaxPooling2D, AveragePooling2D
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten
from tensorflow.keras import optimizers
from keras.regularizers import l2

from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix

from glob import glob
import cv2
import math 
import itertools


def substr(any_string, start, length):  
   
   any_substr = any_string[start-1:start+length-1]
   return any_substr


def fig_drive():
   any_backslash = "\\"
   file_path = os.getcwd() + any_backslash
   drive = substr(file_path,1,1)
   return drive


drive = fig_drive()  # "e"


def plot_confusion_matrix(drive, MODEL, DATASET, nWidth, C_OR_B, nAccuracyPercent,
                          nNodesInFirstLayer, nNodesInSecondLayer, nNodesInThirdLayer,
                          nKernelSize, nNodesInDense, nRegularizerFraction, nDropoutFraction,
                          IncludeBatchNormalization, nTrain, nTest, nEpochs):  

    
   # Let’s also look at the confusion matrix.
   # The confusion matrix is a table which shows the percentage of correct
   # (true positives or true negatives) and incorrect
   # (false positives or false negatives) classifications for each positive
   # (default) or negative (non-default) class. In the table below,
   # the true class is given along the x-axis, while the predicted class
   # is given along the y-axis. Graphically, this looks like:
   
   from sklearn.metrics import confusion_matrix

   csv_path = ""      
   csv_name = "TEST_PRED_" + str(nWidth) + "_X_" + str(nWidth) + "_" + DATASET + ".CSV"  
    
   df = pd.read_csv(csv_path + csv_name, header=0)
   nRows = len(df)  # should be same as nTest

   if 1 == 2:
      print()
      print(df.head(10))
      print()
      print("rows = ", nRows)
   
 
   y_test = df["y_test"].values.tolist()
   y_pred = df["y_pred"].values.tolist()

   nLabels = len(y_test)

   # 10 classes
   list_cols = ['T-shirt_top', 'Trouser', 'Pullover', 'Dress', 'Coat',
                'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
            
   list_rows = list_cols

   nCols = len(list_cols)

   cm = confusion_matrix(y_test, y_pred)     
  
   cm = cm.astype('float')/cm.sum(axis=0)
  
   fig, ax = plt.subplots()

   ax = sns.heatmap(cm, fmt='.2f', linewidths=2, cmap="Blues", alpha=0.90, annot=True)
  
   ax.set_xlabel('\nTrue Classification\n', size=14, fontweight='bold')
   ax.set_ylabel('\nPredicted Classification\n', size=14, fontweight='bold')
   
   fig = plt.gcf()

   fig.set_size_inches(8, 11)  
   
   # turn off the frame
   ax.set_frame_on(False)

   # put the major ticks at the middle of each cell
   ax.set_yticks(np.arange(nCols) + 0.5, minor=False)
   ax.set_xticks(np.arange(nCols) + 0.5, minor=False)

   labels = list_cols

   ax.set_xticklabels(list_cols, minor=False)
   ax.set_yticklabels(list_rows, minor=False)
      
   plt.xticks(rotation= 45, fontweight='bold')  # 90    # IMPROVE rotate x-axis
   plt.yticks(rotation=  0, fontweight='bold')  # ???

   ax.grid(False)

   # Turn off all the ticks
   ax = plt.gca()

   for t in ax.xaxis.get_major_ticks():
      t.tick1On = False
      t.tick2On = False
   for t in ax.yaxis.get_major_ticks():
      t.tick1On = False
      t.tick2On = False
  
   if C_OR_B == "C":
      CLR_OR_BW = "COLOR"  
   elif C_OR_B == "B":
      CLR_OR_BW = "B&W"

   any_title  = "\n" + DATASET + " " + MODEL + " " + CLR_OR_BW + " Confusion Matrix"
   any_title += "  TEST Rows " + str(nRows) 
   any_title += "   Epochs " + str(nEpochs) + "   TEST Accuracy Percent " + str(round(nAccuracyPercent,2)) 
   if MODEL == "1":
      any_title += "\nNodes In 1st & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInDense)
   elif MODEL == "2":
      any_title += "\nNodes In 1st & 2nd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInDense)
   elif MODEL == "3":
      any_title += "\nNodes In 1st 2nd 3rd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInThirdLayer) + " & " + str(nNodesInDense)

   any_title += "  Kernel Size " + str(nKernelSize) 
   any_title += "\nRegularizer Percent " + str(round(nRegularizerFraction * 100,0))
   any_title += "  Dropout Percent " + str(round(nDropoutFraction * 100,0)) 
   any_title += "  BatchNormalize=" + IncludeBatchNormalization    
   
   plt.title(any_title, fontsize=15, fontweight='bold')

   plt.tight_layout()

   save_path = ""

   file_name  = DATASET + "_" + MODEL + "_" + CLR_OR_BW
   file_name += "_1st_Nodes_" + str(nNodesInFirstLayer)
   if MODEL == "2" or MODEL == "3":
      file_name += "_2nd_Nodes_" + str(nNodesInSecondLayer)
   if MODEL == "3":
      file_name += "_3rd_Nodes_" + str(nNodesInThirdLayer)
   file_name += "_Dense_Nodes_" + str(nNodesInDense)
   file_name += "_Kernel_" + str(nKernelSize)
   file_name += "_Reg_Pct_" + str(nRegularizerFraction*100)
   file_name += "_Dropout_Pct_" + str(nDropoutFraction*100)
   file_name += "_Test_Rows_" + str(nRows)
   file_name += "_Epochs_" + str(nEpochs)
   file_name += "_BN_" + IncludeBatchNormalization
 
   file_name += "_Accuracy_" + str(round(nAccuracyPercent,2))
   if MODEL == "1": 
      file_name += "_CM_PAD.PNG"
   else: 
      file_name += "_CM.PNG"
   
   save_image = save_path + file_name + ".png"
   save_pdf = save_path + file_name + ".pdf"
   print("saving heatmap plot")
   plt.tight_layout()

   plt.savefig(save_image, dpi=300, bbox_inches='tight')   
   plt.show()





def plot_acc_loss(nAccuracyPercent, result, nEpochs, MODEL, DATASET, nWidth, C_OR_B,
                  nNodesInFirstLayer, nNodesInSecondLayer, nNodesInThirdLayer,
                  nKernelSize, nNodesInDense, nRegularizerFraction, nDropoutFraction,
                  IncludeBatchNormalization, nTrain, nTest):

    acc = result.history['accuracy']
    loss = result.history['loss']
    val_acc = result.history['val_accuracy']
    val_loss = result.history['val_loss']
    plt.figure(figsize=(15, 5))
    plt.subplot(121)
    plt.plot(range(1,nEpochs), acc[1:], label='Train_acc')
    plt.plot(range(1,nEpochs), val_acc[1:], label='Test_acc')

    if C_OR_B == "C":
      CLR_OR_BW = "COLOR"  
    elif C_OR_B == "B":
      CLR_OR_BW = "B&W"  

    any_title  = DATASET + " " + MODEL + " " + CLR_OR_BW + ' Accuracy Plot (TEST ' + str(round(nAccuracyPercent,2)) + ' Percent)'
    if MODEL == "1":
       any_title += "\nNodes In 1st & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInDense)
    elif MODEL == "2":
       any_title += "\nNodes In 1st & 2nd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInDense)
    elif MODEL == "3":
       any_title += "\nNodes In 1st 2nd 3rd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInThirdLayer) + " & " + str(nNodesInDense)

    any_title += "\nRegularizer Pct " + str(round(nRegularizerFraction * 100,0))
    any_title += "  Dropout Pct " + str(round(nDropoutFraction * 100,0))
    any_title += "  Epochs " + str(nEpochs)    
 
    any_title += "\nKernelSize " + str(nKernelSize)    
    any_title += "  BatchNormalize=" + IncludeBatchNormalization + "  (rows " + str(nTest) + ")"    
 
    plt.title(any_title, size=15)  

    plt.xlabel("Epochs", fontsize=12)
    plt.ylabel("Accuracy", fontsize=12) 
    plt.legend()
    plt.grid(True)
    plt.subplot(122)
    plt.plot(range(1,nEpochs), loss[1:], label='Train_loss')
    plt.plot(range(1,nEpochs), val_loss[1:], label='Test_loss')

    any_title  = DATASET + " " + MODEL + " " + CLR_OR_BW + ' Loss Plot (TEST ' + str(round(nAccuracyPercent,2)) + ' Percent)'
    if MODEL == "1":
       any_title += "\nNodes In 1st & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInDense)
    elif MODEL == "2":
       any_title += "\nNodes In 1st & 2nd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInDense)
    elif MODEL == "3":
       any_title += "\nNodes In 1st 2nd 3rd Layer & Dense " + str(nNodesInFirstLayer) + " & " + str(nNodesInSecondLayer) + " & " + str(nNodesInThirdLayer) + " & " + str(nNodesInDense)
    any_title += "\nRegularizer Pct " + str(round(nRegularizerFraction * 100,0))
    any_title += "  Dropout Pct " + str(round(nDropoutFraction * 100,0))
    any_title += "  Epochs " + str(nEpochs)    
 
    any_title += "\nKernelSize " + str(nKernelSize)    
    any_title += "  BatchNormalize=" + IncludeBatchNormalization + "  (rows " + str(nTest) + ")"    
 
    plt.title(any_title, size=15)  
 
    plt.xlabel("Epochs", fontsize=12)
    plt.ylabel("Loss", fontsize=12) 
    plt.legend()
    plt.grid(True)
    
    print("saving plot")
    file_name  = DATASET + "_" + MODEL + "_" + CLR_OR_BW
    file_name += "_1st_Nodes_" + str(nNodesInFirstLayer)
    if MODEL == "2" or MODEL == "3":
       file_name += "_2st_Nodes_" + str(nNodesInSecondLayer)
    if MODEL == "3":
      file_name += "_3rd_Nodes_" + str(nNodesInThirdLayer)
    file_name += "_Dense_Nodes_" + str(nNodesInDense)
    file_name += "_Kernel_" + str(nKernelSize)
    file_name += "_Reg_Pct_" + str(nRegularizerFraction*100)
    file_name += "_Dropout_Pct_" + str(nDropoutFraction*100)
    file_name += "_Test_Rows_" + str(nTest)
    file_name += "_Epochs_" + str(nEpochs)
    file_name += "_BN_" + IncludeBatchNormalization

    file_name += "_Accuracy_" + str(round(nAccuracyPercent,2)) + "_ACC.PNG"

    plt.tight_layout()
    plt.savefig(file_name, dpi=300, bbox_inched='tight')
    plt.show()

    
#####################################################################################################


DATASET = "FASHION"


nWidth = 28  # pixels

nBatchSize = 10  # 128


cstr  = "\nEnter nEpochs"       # IMPROVE
cstr += "\n< 5> "
cstr += "\n<10> testing FASHION "
cstr += "\n<15> FASHION"
cstr += "\n<20> "
cstr += "\n<25> "
cstr += "\n<30> base"
cstr += "\n<40> ****** "
cstr += "\n<50> "

nEpochs = int(input(cstr)) 

patience =   5   # IMPROVE

if 1 == 2:
   cstr  = "\n\nSelect Color or B&W "
   cstr += "\n<C> Color "
   cstr += "\n<B> B&W "
   C_OR_B = input(cstr)
else:   
   C_OR_B = "B"  # COLOR OR B&W

nClasses = 10
  

cstr  = "\nSelect what subset of FASHION to use "
cstr += "\n<F>  Enter a random fraction of Fashion MNIST "
cstr += "\n<1>  Use  1 percent of data (constant dataset) simplest testing "
cstr += "\n<2>  Use  2 percent of data (constant dataset) "
cstr += "\n<3>  Use  3 percent of data (constant dataset) "
cstr += "\n<5>  Use  5 percent of data (constant dataset) ****** "
cstr += "\n<10> Use 10 percent of data (constant dataset) "
cstr += "\n<20> Use 20 percent of data (constant dataset) "

SUBSET = input(cstr)

if SUBSET == "F":
         
   df_train = pd.read_csv(drive + ':/DATA_FASHION_MNIST/fashion-mnist_train.csv',sep=',')
   df_test = pd.read_csv(drive  + ':/DATA_FASHION_MNIST/fashion-mnist_test.csv', sep = ',')
      
   print("before random shuffle dimension of df_train: {}".format(df_train.shape)) 
   shuffle_fraction_string = input("Enter fraction of df_train to keep :    (0.05) " )
   print()

   shuffle_fraction = float(shuffle_fraction_string)
      
   df_train = df_train.sample(frac=shuffle_fraction)  # random shuffle and keep part of the data
   df_test = df_test.sample(frac=shuffle_fraction)    # random shuffle and keep part of the data
         
   df_train.to_csv("fashion_train_subset.csv", index=False) 
   df_test.to_csv("fashion_test_subset.csv", index=False) 

   input("wait ... created subset CSVs")

elif SUBSET == "1":
   df_train = pd.read_csv("fashion_train_01.csv", header=0)
   df_test = pd.read_csv("fashion_test_01.csv", header=0)
         
elif SUBSET == "2":
   df_train = pd.read_csv("fashion_train_02.csv", header=0)
   df_test = pd.read_csv("fashion_test_02.csv", header=0)
         
elif SUBSET == "3":
   df_train = pd.read_csv("fashion_train_03.csv", header=0)
   df_test = pd.read_csv("fashion_test_03.csv", header=0)
         
elif SUBSET == "5":
   df_train = pd.read_csv("fashion_train_05.csv", header=0)
   df_test = pd.read_csv("fashion_test_05.csv", header=0)
         
elif SUBSET == "10":
   df_train = pd.read_csv("fashion_train_10.csv", header=0)
   df_test = pd.read_csv("fashion_test_10.csv", header=0)
         
elif SUBSET == "20":
   df_train = pd.read_csv("fashion_train_20.csv", header=0)
   df_test = pd.read_csv("fashion_test_20.csv", header=0)       

nTrain = len(df_train)
nTest  = len(df_test)
 
print("df_train rows included", nTrain)
print("df_test  rows included", nTest)
print()


NEW_OR_OLD = "OLD"

   
if NEW_OR_OLD == "OLD":

   np_train = np.array(df_train, dtype = 'float32')
   np_test = np.array(df_test, dtype='float32')
   
   x_train = np_train[:,1:]/255  # normalize to 0-1
   y_train = np_train[:,0]

   x_test = np_test[:,1:]/255    # normalize to 0-1
   y_test = np_test[:,0]

   
   print(df_train.head(5))
   #input("wait")

   print("df_train.label.unique()")
   print(df_train.label.unique())

   print("df_train['label'].value_counts()")
   print(df_train['label'].value_counts())

   sns.countplot(df_train['label'])
   plt.title("TRAIN " + SUBSET)
   plt.tight_layout()
   plt.show()

   sns.countplot(df_test['label'])
   plt.title("TEST " + SUBSET)
   plt.tight_layout()
   plt.show()

   if C_OR_B == "B":     # B&W
      x_train = x_train.reshape(-1, nWidth, nWidth, 1)
      if 1 == 1:
         x_test  = x_test.reshape(-1, nWidth, nWidth, 1)
      else:   
         x_test  = np.expand_dims(x_test, axis=-1)
   elif C_OR_B == "C":     # Color
      x_train = np.reshape(x_train, (28, 3, 28, 3)) 
      x_test  = np.reshape(x_test, (28, 3, 28, 3)) 

   #display(x_test, "x_test")
   #print()
   #display(y_test, "y_test")
     

   class_names = ['T_shirt_top', 'Trouser', 'Pullover', 'Dress', 'Coat', 
                  'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

   plt.figure(figsize=(10, 10))
   for i in range(36):
      plt.subplot(6, 6, i + 1)
      plt.xticks([])
      plt.yticks([])
      plt.grid(False)
      #plt.imshow(x_train[i].reshape((28,28)))
      plt.imshow(x_train[i].reshape((28,28)), cmap=plt.cm.binary)
      label_index = int(y_train[i])
      plt.title(class_names[label_index])
      plt.suptitle("64 Sample B&W Images (28 x 28 pixels) from Fashion MNIST Dataset")
 
   plt.show()
  

   
cstr  = "\nSelect Number Of CNN Layers "
cstr += "\n<1> ******"
cstr += "\n<2> "
cstr += "\n<3> "
MODEL = input(cstr)     

print("MODEL = ", MODEL)
   
  
cstr  = "\nNodes in First Layer "
cstr += "\n<32> "
cstr += "\n<64> ******"
cstr += "\n<96> "
nNodesInFirstLayer = int(input(cstr))


if MODEL == "1":
   nNodesInSecondLayer = nNodesInFirstLayer
else:         
   cstr  = "\nNodes in Second Layer "
   cstr += "\n<32> "
   cstr += "\n<64> ******"
   cstr += "\n<96> "
   nNodesInSecondLayer = int(input(cstr))

if MODEL == "1" or MODEL == "2":
   nNodesInThirdLayer = nNodesInSecondLayer
else:         
   cstr  = "\nNodes in Third Layer "
   cstr += "\n<32> "
   cstr += "\n<64> ****** "
   cstr += "\n<96> "
   nNodesInThirdLayer = int(input(cstr))


          
cstr  = "\nKernel Size "
cstr += "\n<3> *** when MODEL = 3 *** ******"
cstr += "\n<4> "
cstr += "\n<5> *** when MODEL = 2 maybe  "
cstr += "\n<7>  "
cstr += "\n<9> "
nKernelSize = int(input(cstr))


        
# activation function for the layer is 'relu' # Rectified Linear Activation
cstr  = "\nRegularizer Percent "
cstr += "\n<1>  "
cstr += "\n<2> "
cstr += "\n<3> ******"
cstr += "\n<4> "
cstr += "\n<5> "
nRegularizerFraction = float(input(cstr)) / 100.0


                      
cstr  = "\nDropout Percent "
cstr += "\n< 5> "
cstr += "\n<10> "
cstr += "\n<12> "
cstr += "\n<15> "
cstr += "\n<17> "
cstr += "\n<20> ****** "
cstr += "\n<30> "
nDropoutFraction = float(input(cstr)) / 100.0


    
# could be 16 or 32 or 128
cstr  = "\nDense Nodes (First Hidden Layer) "
cstr += "\n< 16> "
cstr += "\n< 32> "
cstr += "\n< 48> "
cstr += "\n< 64> "
cstr += "\n< 96> "
cstr += "\n<112> "
cstr += "\n<128> ****** "
cstr += "\n<144> "
cstr += "\n<160> "
nNodesInDense = int(input(cstr))


    
# We add Batch Normalization where we acheive Zero mean and Variance one.
# It scales down outliers and forces the network to learn features in a
# distributed way, not relying too much on a Particular Weight and makes
# the model better Generalize the Images.
cstr  = "\nInclude Batch Normalization "
cstr += "\n<Y> Yes ****** "
cstr += "\n<N> No  "
IncludeBatchNormalization = input(cstr)


print("Setting up model")
print()

   
model = Sequential()
   
      
if NEW_OR_OLD == "OLD":
         
   # convolutional layer to deal with input images which are 2-dimensional matrices
   if C_OR_B == "C":       # Color
      model.add(Conv2D(nNodesInFirstLayer,(nKernelSize, nKernelSize), kernel_regularizer=l2(nRegularizerFraction),
                       activation='relu', input_shape=(nWidth,nWidth,3)))
   elif C_OR_B == "B":     # B&W
      if MODEL == "2" or MODEL == "3":
         model.add(Conv2D(nNodesInFirstLayer,(nKernelSize, nKernelSize), kernel_regularizer=l2(nRegularizerFraction),
                          activation='relu', input_shape=(nWidth,nWidth,1)))
      elif MODEL == "1":
         model.add(Conv2D(nNodesInFirstLayer,(nKernelSize, nKernelSize),
                          kernel_regularizer=l2(nRegularizerFraction),
                          activation='relu', input_shape=(nWidth,nWidth,1),
                          strides=1, padding='same', data_format='channels_last'))
      
   if IncludeBatchNormalization == "Y":
      model.add(BatchNormalization())
      model.add(Dropout(nDropoutFraction))
  
   model.add(MaxPooling2D((2, 2)))
   model.add(Dropout(nDropoutFraction))

   if MODEL == "2" or MODEL == "3": 
      model.add(Conv2D(nNodesInSecondLayer, (nKernelSize, nKernelSize), kernel_regularizer=l2(nRegularizerFraction), activation='relu'))
      model.add(MaxPooling2D((2, 2)))
      model.add(Dropout(nDropoutFraction))

   if MODEL == "3":   
      model.add(Conv2D(nNodesInThirdLayer,(nKernelSize, nKernelSize), kernel_regularizer=l2(nRegularizerFraction), activation='relu', input_shape=(nWidth,nWidth,1)))
      if IncludeBatchNormalization == "Y":
         model.add(BatchNormalization())
         model.add(Dropout(nDropoutFraction))
  
      model.add(MaxPooling2D((2, 2)))

      model.add(Dropout(nDropoutFraction))
       
   # Flatten layer serves as a connection between the convolution and dense layers   
   model.add(Flatten())

   model.add(Dense(nNodesInDense, kernel_regularizer=l2(nRegularizerFraction), activation='relu'))

   if IncludeBatchNormalization == "Y":
      model.add(BatchNormalization())
     
   model.add(Dropout(nDropoutFraction))
   
   if MODEL == "3":   
      model.add(Dense(nNodesInDense, kernel_regularizer=l2(nRegularizerFraction), activation='relu'))

      if IncludeBatchNormalization == "Y":
         model.add(BatchNormalization())
     
      model.add(Dropout(nDropoutFraction))


   # nClasses is number of outcomes
   # softmax makes the output sum up to 1 so the output can be interpreted as probabilities
   # model will makes it prediction based on which option has the highest probability
   model.add(Dense(nClasses, kernel_regularizer=l2(nRegularizerFraction), activation='softmax'))
           
   print("Compiling model")

   # compile the model
   # takes three parameters: optimizer, loss and metrics
   # optimizer controls the learning rate (adam)
   # learning rate determines how fast the optimal weights for the model are calculated
   # a smaller learning rate may lead to more accurate weights (up to a certain point)
   # but the time it takes to compute the weights will be longer
   # loss function : sparse_categorical_crossentropy, categorical_crossentropy
   # metric: accuracy

   if 1 == 1:      
      #model.compile(loss=tf.keras.losses.categorical_crossentropy,
      model.compile(loss=tf.keras.losses.sparse_categorical_crossentropy,
                    optimizer=tf.keras.optimizers.Adam(lr=0.0001, decay=1e-6),
                    metrics=['accuracy'])
   elif 1 == 1:  # MODEL == "2":
      model.compile(loss=keras.losses.categorical_crossentropy,
                    optimizer=keras.optimizers.Adam(),
                    metrics=['accuracy'])

   print(model.summary())
     
   filepath = MODEL + "_" + DATASET + "_weights.hdf5"

         
   if 1 == 1:       # checkpoint ... save results to hdf5 file
      mode = 'max'  # 'auto'
      verbose = 1   # 1
      checkpointer = ModelCheckpoint(filepath=MODEL + "_" + DATASET + '_weights.hdf5', monitor='val_accuracy',
                                     verbose=verbose, save_best_only=True,
                                     save_weights_only=False, mode=mode, period=1)
      # training the model
      # nEpochs: number of times the model will cycle through the data
      history = model.fit(x_train, y_train, batch_size=nBatchSize, epochs=nEpochs, verbose=verbose,
                          validation_split=0.2, callbacks=[checkpointer])
  
   elif 1 == 2:     # checkpoint ...
      mode = 'min'  # 'auto'
      verbose = 1   # 1
      checkpointer = ModelCheckpoint(filepath=MODEL + "_" + DATASET + '_weights.hdf5', monitor='val_loss', verbose=verbose,
                                     save_best_only=True, save_weights_only=False, mode=mode, period=1)
      
      #early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=patience)    
      #history = model.fit(x_train, y_train, batch_size=10, epochs=nEpochs, verbose=1, validation_split=0.2,
      #                   callbacks=early_stopping)
      history = model.fit(x_train, y_train, batch_size=nBatchSize, epochs=nEpochs, verbose=1,
                          validation_data=(x_test, y_test), callbacks=[checkpointer])

   elif 1 == 2: # patience and 1 percent improvement 
      early_stopping = EarlyStopping(monitor='val_accuracy', mode='max', verbose=1, min_delta=1, patience=patience)    
      history = model.fit(x_train, y_train, batch_size=nBatchSize, epochs=nEpochs, verbose=1, validation_split=0.2,
                          callbacks=early_stopping)
      
   elif 1 == 2: # patience
      early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=patience)    
      history = model.fit(x_train, y_train, batch_size=nBatchSize, epochs=nEpochs, verbose=1, validation_split=0.2,
                          callbacks=early_stopping)
      
   elif 1 == 2: # WORKS more accurate
      history = model.fit(x_train, y_train, batch_size=nBatchSize, epochs=nEpochs, verbose=1, validation_split=0.2,
                          callbacks=EarlyStopping(monitor='val_loss'))
      
   elif 1 == 1: # WORKS less accurate
      history = model.fit(x_train, y_train, validation_data=(x_test, y_test), batch_size=nBatchSize, epochs=nEpochs, verbose=1,
                          callbacks=EarlyStopping(monitor='val_loss'))



   nHistory = len(history.history['loss'])  # Only nHistory epochs are run. 
   print("nHistory (epochs) = ", nHistory)



# Assuming loaded_model is your model definition before compilation
# Load Weights
model.load_weights(MODEL + "_" + DATASET + "_weights.hdf5")



if NEW_OR_OLD == "OLD":

   print()
   print("TRAIN:")   # display TRAIN summary results 

   # train evaluate
   scores = model.evaluate(x_train, y_train, verbose=0)
   print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
   print()

   # train predict
   y_train_pred = np.argmax(model.predict(x_train), axis = 1)

   # train classification report
   print(classification_report(y_train, y_train_pred))

   # train confusion matrix
   print(confusion_matrix(y_train, y_train_pred))      

   print()     
   print("TEST:")    # display TEST summary results

   # test evaluate
   scores = model.evaluate(x_test, y_test, verbose=0)
   nAccuracyPercent = round(scores[1] * 100,3)
   print()
   print("nAccuracyPercent = ", nAccuracyPercent)
      
   print()
   print("scores")
   print(scores)
   print()
      
   print("model.metrics_names")
   print(model.metrics_names)
   print()
     
   print("model.metrics_names[0]", model.metrics_names[0])
   print("model.metrics_names[1]", model.metrics_names[1])
   print()
            
   print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
   print()

   # test predict
   y_test_pred = np.argmax(model.predict(x_test), axis = 1)

   # test classification report
   print(classification_report(y_test, y_test_pred))

   # test confusion matrix
   print(confusion_matrix(y_test, y_test_pred))         
  
   print("type(history)")
   print(type(history))
   print()
   input("wait")

   # pass all input parameters
   plot_acc_loss(nAccuracyPercent, history, nHistory, MODEL, DATASET, nWidth, C_OR_B,
                 nNodesInFirstLayer, nNodesInSecondLayer, nNodesInThirdLayer, nKernelSize,
                 nNodesInDense, nRegularizerFraction, nDropoutFraction,
                 IncludeBatchNormalization, nTrain, nTest)
    
   
   list_y_test = y_test.tolist()
   list_y_pred = y_test_pred.tolist()
   df = pd.DataFrame()
   df["y_test"] = list_y_test
   df["y_pred"] = list_y_pred

   # save results of y_test and y_pred
   save_csv = "TEST_PRED_" + str(nWidth) + "_X_" + str(nWidth) + "_" + DATASET + ".CSV"  
   df.to_csv(save_csv, index=False)

   # pass all input parameters
   plot_confusion_matrix(drive, MODEL, DATASET, nWidth, C_OR_B, nAccuracyPercent,
                         nNodesInFirstLayer, nNodesInSecondLayer, nNodesInThirdLayer, nKernelSize,
                         nNodesInDense, nRegularizerFraction, nDropoutFraction,
                         IncludeBatchNormalization, nTrain, nTest, nEpochs)   # IMPROVE

     
input("DONE")
    






